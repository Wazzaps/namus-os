// Various constants
#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)
#define KER_VERSION "00:" STR(BUILD_NUM)
#define TOHEX(a) ((a) <= 9 ? (a) + '0' : (a) + 'A' - 10)
#define ISGRAPH(a) ((a) >= ' ' && (a) != 127)

void copy_char_array(char src[], char dst[], int length);
int streq(char a[], char b[], int length);

static inline void delay(int32_t count)
{
	asm volatile("__delay_%=: subs %[count], %[count], #1; bne __delay_%=\n"
		 : "=r"(count): [count]"0"(count) : "cc");
}