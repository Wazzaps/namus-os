#include <stddef.h>
#include <stdint.h> 

#include "utils.h"
#include "mmio.h"
#include "uart.h"

#include "utils.c"
#include "mmio.c"
#include "uart.c"

void kernel_main(uint32_t r0, uint32_t r1, uint32_t atags)
{
	// Declare as unused
	(void) r0;
	(void) r1;
	(void) atags;
	
	uart_init();
	uart_puts("*********************************\n* NAMUS OS - Version "KER_VERSION" *\n*********************************\n");

	while(1);
}