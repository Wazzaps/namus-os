
void copy_char_array(char src[], char dst[], int length) {
    for (int i = 0; i < length; i++) {
        dst[i] = src[i];
    }
}

int streq(char a[], char b[], int length) {
    for (int i = 0; i < length; i++) {
        if (a[i] != b[i])
            return 0;
        if (a[i] == '\0' && b[i] == '\0')
            return 1;
        if (a[i] == '\0' || b[i] == '\0')
            return 0;
    }
    return 1;
}