SRCFILES := $(wildcard ksrc/*)
KMODS := $(wildcard kmodules/*)
BUILDNUM  := $(shell git rev-parse --short --verify HEAD)

# Compiler lines
LD := arm-none-eabi-gcc -ffreestanding -O2 -nostdlib
CC := arm-none-eabi-gcc -mcpu=arm1176jzf-s -fpic -ffreestanding -std=gnu99 -O2 -Wall -Wextra -pedantic
AC := arm-none-eabi-gcc -mcpu=arm1176jzf-s -fpic -ffreestanding

# Final output
kbin/kernel.bin: kbin/kernel.elf
	arm-none-eabi-objcopy kbin/kernel.elf -O binary kbin/kernel.bin

kbin/kernel.elf: kobj/kernel.o kobj/bootstrap.o
	$(LD) -T linker.ld -o kbin/kernel.elf kobj/kernel.o kobj/bootstrap.o

# Kernel object
kobj/kernel.o: ksrc/kernel.c
	$(CC) -c $< -o $@ -DBUILD_NUM=$(BUILDNUM)

# Bootstrap asm
kobj/bootstrap.o: ksrc/bootstrap.s
	$(AC) -c $< -o $@

# KModules
kmods:
	@$(foreach mkfile,$(KMODS),$(MAKE) -s -f ../../ModMakefile -C $(mkfile);)

# etc
clean:
	rm -f kobj/*.o
	rm -f kbin/*.bin
	rm -f kbin/*.elf