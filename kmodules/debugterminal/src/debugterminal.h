struct DEBUGTERMINAL_API {
    void (*add_handler)(char name[64], void (*func)());
    void (*process_line)();
}

#define DEBUGTERMINAL_MAXHANDLERS 12

struct DEBUGTERMINAL_handler {
    char name[64];
    void (*func)();
};