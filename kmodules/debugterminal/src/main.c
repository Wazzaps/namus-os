#include "utils.h"
#include "uart.h"
#include "debugterminal.h"

struct KMODSTRUCT __attribute__((section (".KMODSTRUCT"))) = {
    .init = init;
}

struct DEBUGTERMINAL_API __attribute__((section (".KMODAPI"))) = {
    .add_handler = add_handler;
    .process_line = process_line;
}

struct DEBUGTERMINAL_handler handlers[DEBUGTERMINAL_MAXHANDLERS];
int current_handler = 0;
char line[64];

////////////////////////
//  Command handlers  //
////////////////////////

void _cmd_version () {
    uart_puts("Version: "KER_VERSION"\n");
}


////////////////////////////
//  End command handlers  //
////////////////////////////


void init(struct OS_API) {
    debugterminal_add_handler("version", _cmd_version);

    while(1)
        process_line();
}

void add_handler(char name[64], void (*func)()) {
    if (current_handler < DEBUGTERMINAL_MAXHANDLERS) {
        copy_char_array(name, handlers[current_handler].name, 64);
        handlers[current_handler].func = func;
        current_handler++;
    }
}

void process_line() {
    char c;
    int i;
    uart_putc('>');
    uart_putc(' ');

    for (i = 0; i < 64; i++) {
        line[i] = 0;
    }

    i = 0;

    while((c = uart_getc()) != '\r' && i < 64) {
        if (ISGRAPH(c))
            uart_putc(c);
        else if (c == 127) {
            if (i > 0) {
                //uart_putc( + '0');
                if (ISGRAPH(line[i-1])) {//line[i-1] >= ' ' && line[i-1] != 127) {
                    uart_puts("\b \b");
                } else {
                    uart_puts("\b \b\b \b\b \b\b \b");
                }
                line[--i] = '\0';
                
            }
            continue;
        } else {
            uart_putc('<');
            uart_putc(TOHEX(c >> 4));
            uart_putc(TOHEX(c & 0xF));
            uart_putc('>');
        }
        line[i++] = c;
    }

    for (int h = 0; h < current_handler; h++) {
        if (streq(line, handlers[h].name, 64)) {
            uart_putc('\r');
            uart_putc('\n');
            handlers[h].func();
            return;
        }
    }

    uart_puts("\r\nUnknown command\r\n");
    
}