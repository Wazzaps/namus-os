#!/bin/bash

while :
    do
    echo
    echo "==== Make ===="
    make clean > /dev/null
    make
    echo "=============="
    echo
    ./qemu/arm-softmmu/qemu-system-arm -m 256M -M raspi2 -serial stdio -kernel kbin/kernel.elf
    sleep 0.5
done